require 'rails_helper'

RSpec.describe Potepan::HomeController, type: :controller do
  before do
    get :index
  end

  it "responds top page" do
    expect(response).to be_successful
    expect(response).to render_template "potepan/home/index"
  end
end
