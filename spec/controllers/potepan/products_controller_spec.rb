require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let(:product) { create(:product) }
    let!(:product_property) { create(:product_property, product: product, value: "type") }

    before do
      get :show, params: { id: product.id }
    end

    it "returns http success" do
      expect(response).to be_successful
    end

    it "assign instance product" do
      expect(assigns(:product)).to eq product
    end

    it "render show template" do
      expect(response).to render_template(:show)
    end

    it "assign instance product_properties" do
      expect(assigns(:product).product_properties).to match_array product_property
    end
  end
end
