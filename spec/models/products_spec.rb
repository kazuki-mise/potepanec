require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "related_products" do
    let!(:taxon1) { create(:taxon) }
    let!(:taxon2) { create(:taxon) }
    let!(:taxon3) { create(:taxon) }
    let!(:taxon4) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon1, taxon2, taxon3]) }
    let!(:product1) { create(:product, taxons: [taxon1]) }
    let!(:product2) { create(:product, taxons: [taxon2]) }
    let!(:product3) { create(:product, taxons: [taxon3]) }
    let!(:product4) { create(:product, taxons: [taxon4]) }

    it "check related_products method" do
      expect(product.related_products).to contain_exactly(product1, product2, product3)
      expect(product.related_products).not_to contain_exactly(product4)
    end
  end
end
