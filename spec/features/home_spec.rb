require 'rails_helper'

RSpec.feature "Home", type: :feature do
  let!(:taxonomy) { create(:taxonomy) }
  let!(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { FactoryBot.create(:product, available_on: Time.current.ago(5.months), taxons: [taxon]) }

  before do
    visit potepan_root_path
  end

  scenario "display new product" do
    within (".featuredProducts") do
      expect(page).to have_content product.name
    end
  end

  scenario "link to product page" do
    click_on product.name
    expect(page).to have_current_path(potepan_product_path(product.id))
  end
end
