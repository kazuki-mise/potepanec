require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let!(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:taxonomy) { create(:taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  scenario "categories/show has taxon product" do
    visit potepan_category_path(taxon.id)
    expect(page).to have_content taxonomy.name
    expect(page).to have_content taxon.name
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price

    click_on product.name
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
  end
end
